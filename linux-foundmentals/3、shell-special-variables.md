## shell 脚本中的特殊变量

这篇文章总结一下 shell 脚本中的特殊变量。



| 变量名称 |                           变量含义                           |
| :------: | :----------------------------------------------------------: |
|    $0    | 当前执行的 shell 脚本文件名，如果执行时包含路径，那么变量也带路径 |
|    $n    | 当前脚本执行的第 n 个参数，如果 n > 9 ，那使用时用大括号引起来，{$10} |
|    $#    |              当前脚本执行时传递的参数总个数              |
|    $@    |                      当前脚本执行时传递的所有参数，                                        |
|    $$    |              当前执行的 shell 脚本的进程号（PID）               |
|    $_    |                 获得上一个命令的最后一个参数                 |
|    $!    |                  获取上一次执行脚本的进程号                  |
|    $?    |                                                              |


