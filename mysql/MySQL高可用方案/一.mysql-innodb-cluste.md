# 简介





MySQL InnoDB Cluster 是 MySQL 官方推出完整的高可用性解决方案。

MySQL InnoDB Cluster 由三大组件组成：**MySQL Shell**，**MySQL Router**，**MySQL Group Replication**。

每个MySQL服务器实例都运行 MySQL Group Replication，它提供了在 InnoDB 集群内复制数据的机制，具有内置故障转移功能。



**MGR**

MGR 是 MySQL Group Replication 的缩写，是MySQLServer5.7.17及更高版本提供的一个内置MySQL插件（Replication）。

MySQL 组复制提供了一个高可用、高弹性、高可靠性的 MySQL 服务。



